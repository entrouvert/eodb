import matplotlib.pyplot as plt
import numpy as np

from .common import GraphCommand


import random
import numpy as np

# Fixing random state for reproducibility
np.random.seed(19680801)


class Command(GraphCommand):

    def handle(self, *args, **options):
        data = []
        zones = []
        for i, (legend, serie) in enumerate(self.get_series(options)):
            zones.append(legend)
            events = []
            data.append(events)
            for event in serie:
                event_date = self.get_event_datetime(event, options)
                hour = event_date.hour
                if hour < 5:
                    hour += 24
                minute = event_date.minute
                events.append(hour + minute / 60.0)

        plt.figure()
        ax = plt.subplot()

        plt.violinplot(data, widths=0.7,
                     showmeans=True, showextrema=True, showmedians=True)

        ax.set_xticks(range(1, len(zones)+1))
        ax.set_xticklabels(zones)
        ax.set_yticks(range(5, 29))
        ax.set_yticklabels(list(range(5, 24)) + list(range(0, 5)))

        self.plot(options)
