import datetime
import email.utils
import mailbox
import time

from django.core.management.base import BaseCommand
from django.utils.dateparse import parse_datetime

from eodb.events.models import Email


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('maildir', nargs='+')

    def handle(self, *args, **options):
        for maildir in options.get('maildir'):
            if 'commits' in maildir:
                continue
            if options['verbosity'] >= 2:
                print(maildir)
            box = mailbox.Maildir(maildir, create=False)
            count = 0
            for message_id in box.iterkeys():
                message = box[message_id]
                try:
                    list_id = message['list-id'].strip('<>').replace('.listes.entrouvert.com', '')
                except (KeyError, AttributeError):
                    print('failed to get list id', message['date'], message['subject'])
                    continue
                try:
                    author_email = email.utils.parseaddr(message['From'])[1]
                except TypeError:
                    continue
                author_date = email.utils.parsedate_to_datetime(message['date'])

                msg, created = Email.objects.get_or_create(msgid=message_id,
                        defaults={
                            'list_id': list_id,
                            'author_email': author_email,
                            'author_datetime': author_date})
                count += 1
            if options['verbosity'] >= 2:
                print('  ->', count)
