import datetime
import matplotlib
import matplotlib.dates
import matplotlib.pyplot as plt
import numpy as np

from .common import GraphCommand


class Command(GraphCommand):
    def add_arguments(self, parser):
        super(Command, self).add_arguments(parser)
        parser.add_argument('--groupby', metavar='TIME UNIT', default='weeks',
                            help='aggregate over weeks (default) / months / years')
        parser.add_argument('--modulecount', type=int,
                            help='count number of modules instead of commits '
                                 '(value is minimum number of commits to be counted in)')
        parser.add_argument('--barwidth', type=int, default=20)

    def handle(self, *args, **options):
        title = 'Git activity'

        total_events = {}
        plots = []
        for i, (legend, serie) in enumerate(self.get_series(options)):
            events = {}
            for commit in serie:
                commit_date = self.get_event_datetime(commit, options).date()
                if options.get('groupby') == 'weeks':
                    graph_date = commit_date - datetime.timedelta(days=commit_date.weekday())
                elif options.get('groupby') == 'months':
                    graph_date = commit_date.replace(day=1)
                elif options.get('groupby') == 'years':
                    graph_date = commit_date.replace(month=1, day=1)

                date = matplotlib.dates.date2num(graph_date)
                if options.get('modulecount'):
                    if not date in events:
                        events[date] = {}
                    if not commit.module in events[date]:
                        events[date][commit.module] = 0
                    events[date][commit.module] += 1
                else:
                    if not date in events:
                        events[date] = 0
                    events[date] += 1

            dates = sorted(events.keys())
            if options.get('modulecount'):
                values = [len([y for y in events[x].values() if y > options.get('modulecount')]) for x in dates]
            else:
                values = [events[x] for x in dates]

            bottoms = [total_events.get(x, 0) for x in dates]
            plot = plt.bar(dates, values, width=options.get('barwidth'), bottom=bottoms)
            plots.append((legend, plot))

            for key, value in events.items():
                if not key in total_events:
                    total_events[key] = 0
                total_events[key] += value

        if i > 1:
            plt.legend([plot[0] for legend, plot in plots],
                       [legend for legend, plot in plots])

        plt.gca().xaxis.set_major_locator(matplotlib.dates.YearLocator())
        plt.gca().xaxis.set_minor_locator(matplotlib.dates.MonthLocator())
        plt.gca().xaxis.set_major_formatter(matplotlib.dates.DateFormatter('%m/%Y'))
        self.plot(options)
