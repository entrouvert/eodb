import datetime

import matplotlib
import matplotlib.colors
import matplotlib.dates
import matplotlib.pyplot as plt

from django.utils.dateparse import parse_date

from .common import GraphCommand


class Command(GraphCommand):
    def add_arguments(self, parser):
        super(Command, self).add_arguments(parser)
        parser.add_argument('--opacity', type=float, default=0.3)
        parser.add_argument('--density', action='store_true')

    def handle(self, *args, **options):
        event_dates = []
        event_times = []
        plots = []
        for i, (legend, serie) in enumerate(self.get_series(options)):
            for commit in serie:
                graph_date = self.get_event_datetime(commit, options)
                graph_date -= datetime.timedelta(hours=4)
                event_dates.append(matplotlib.dates.date2num(graph_date.date()))
                event_times.append(graph_date.hour + graph_date.minute / 60. + graph_date.second / 3600)
            if options.get('density'):
                plot = plt.hist2d(event_dates, event_times, bins=(100, 100),
                        cmap=plt.cm.jet, norm=matplotlib.colors.LogNorm())
                plt.colorbar()
            else:
                plot = plt.scatter(event_dates, event_times, alpha=options.get('opacity'))
            plots.append((legend, plot))

        if i > 1:
            plt.legend([plot for legend, plot in plots],
                       [legend for legend, plot in plots])

        if options.get('datemin'):
            datemin = parse_date(options['datemin'])
        else:
            datemin = datetime.date(2000, 1, 1)  # past
        if options.get('datemax'):
            datemax = parse_date(options['datemax'])
        else:
            datemax = datetime.date.today()

        plt.gca().xaxis.set_major_locator(matplotlib.dates.YearLocator())
        plt.gca().xaxis.set_minor_locator(matplotlib.dates.MonthLocator())
        plt.gca().xaxis.set_major_formatter(matplotlib.dates.DateFormatter('%m/%Y'))
        if (datemax - datemin).days < 365:
            plt.gca().xaxis.set_minor_formatter(matplotlib.dates.DateFormatter('%m/%Y'))

        plt.gca().set_ylim([0, 24])
        plt.yticks(range(24), list(range(4, 24)) + list(range(0, 4)))
        plt.grid()
        self.plot(options)
