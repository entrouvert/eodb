import csv

from django.core.management.base import BaseCommand
from django.utils.dateparse import parse_datetime
from django.utils.timezone import make_aware

import pytz

from eodb.events.models import Redmine


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('filename', metavar='FILENAME')

    def handle(self, *args, **options):
        for row in csv.reader(open(options['filename'])):
            if not row[2]:
                continue
            try:
                redmine, created = Redmine.objects.get_or_create(journal_id=int(row[0]),
                        defaults={
                            'author_datetime': make_aware(parse_datetime(row[1])),
                            'module': row[2],
                            'author_email': row[3]})
            except pytz.exceptions.Error:
                # ignore that one
                continue
