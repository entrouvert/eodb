# -*- coding: utf-8 -*-

import os
import subprocess
import time

from django.core.management.base import BaseCommand
from django.utils.dateparse import parse_datetime

from eodb.events.models import Commit

author_map = {
    'fpeters@0d.be': 'fpeters@entrouvert.com',
    'jschneider@entrouvrt.org': 'jschneider@entrouvert.com',
    'bdauvergne@ciboulette.entrouvert.og': 'bdauvergne@entrouvert.com',
    'bdauvergne@entrouvert.org': 'bdauvergne@entrouvert.com',
    'bdauvergne@moria.entrouvert.org': 'bdauvergne@entrouvert.com',
    'bdauvergne@lupin.entrouvert.com': 'bdauvergne@entrouvert.com',
    'benjamin.dauvergne@a716ebb1-153a-0410-b759-cfb97c6a1b53': 'bdauvergne@entrouvert.com',
    'mikael.ates@gmail.com': 'mates@entrouvert.com',
    'mikael@xyzstation.(none)': 'mates@entrouvert.com',
    'serghei@hekla.home': 'smihai@entrouvert.com',
    'serghei.mihai@devlibre.net': 'smihai@entrouvert.com',
    'Damien Laniel@localhost': 'dlaniel@entrouvert.com',
    'elias@showk.me': 'eshowk@entrouvert.com',
    'zebuline@entrouvert.com': 'lguerin@entrouvert.com',
}

class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('repository', nargs='+')

    def handle(self, *args, **options):
        t0 = time.time()
        basedir = os.getcwd()
        for repository in sorted(options.get('repository')):
            os.chdir(basedir)
            try:
                os.chdir(repository)
            except OSError:
                continue
            module = os.path.basename(repository.rstrip('/')).replace('.git', '')
            print(module)
            try:
                for line in subprocess.check_output(['git', 'log', '--format=format:%H👾%an👾%ae👾%aI👾%cI👾%s']).splitlines():
                    try:
                        sha, author_name, author_email, author_date, commit_date, subject = line.decode('utf-8').strip().split('👾')
                    except ValueError:
                        continue
                    if not author_email:
                        author_email = author_name + '@entrouvert.com'
                    author_email = author_map.get(author_email, author_email)
                    commit, created = Commit.objects.get_or_create(sha=sha, defaults={
                        'module': module,
                        'author_datetime': parse_datetime(author_date),
                        'commit_datetime': parse_datetime(commit_date),
                        'author_email': author_email,})
                    if commit.author_email != author_email:
                        commit.author_email = author_email
                        commit.save()
            except subprocess.CalledProcessError as e:
                print('  ', e)

        print(time.time() - t0)
