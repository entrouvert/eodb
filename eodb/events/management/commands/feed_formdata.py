from django.core.management.base import BaseCommand
from django.utils.dateparse import parse_datetime
from django.db import transaction

from eodb.events.models import Formdata


class Command(BaseCommand):

    def add_arguments(self, parser):
        parser.add_argument('filename', metavar='FILENAME')

    def handle(self, *args, **options):
        transaction.set_autocommit(False)
        for i, row in enumerate(open(options['filename'])):
            datetime = parse_datetime(row)
            if not datetime:
                continue
            Formdata(author_datetime=datetime).save()
            if i % 1000 == 0:
                transaction.commit()
