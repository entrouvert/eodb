from django.db import models



class Commit(models.Model):
    sha = models.CharField(max_length=41)
    module = models.CharField(max_length=50)
    author_email = models.CharField(max_length=100)
    author_datetime = models.DateTimeField()
    commit_datetime = models.DateTimeField()


class Email(models.Model):
    msgid = models.CharField(max_length=100, unique=True)
    list_id = models.CharField(max_length=100)
    author_email = models.CharField(max_length=100)
    author_datetime = models.DateTimeField()


class Redmine(models.Model):
    journal_id = models.IntegerField(unique=True)
    module = models.CharField(max_length=200)
    author_email = models.CharField(max_length=100)
    author_datetime = models.DateTimeField()


class Formdata(models.Model):
    # add formdata_id (site:formdata_id)
    author_datetime = models.DateTimeField()
